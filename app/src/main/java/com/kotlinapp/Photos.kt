package com.kotlinapp

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson

//data class Photos {
//
//    var id: Int? = null
//    var title: String? = null
//    var thumbnailUrl: String? = null
//
//    constructor(id: Int, title: String, content: String) {
//        this.id = id
//        this.title = title
//        this.thumbnailUrl = content
//    }
//}

data class Photos(
        val id: Int,
        val title: String,
        val thumbnailUrl: String
){
    class Deserializer: ResponseDeserializable<Array<Photos>> {
        override fun deserialize(content: String): Array<Photos>? = Gson().fromJson(content, Array<Photos>::class.java)
    }
}
package com.kotlinapp

import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.ListView
import android.widget.TextView
import com.github.kittinunf.fuel.httpGet
import java.net.URL


class MainActivity : AppCompatActivity() {

    private var photosList = ArrayList<Photos>()
    private val URL = "https://jsonplaceholder.typicode.com/photos"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        URL.httpGet().responseObject(Photos.Deserializer()) { request, response, result ->
            val (photos, err) = result
            photos?.forEach { photo ->
                photosList.add(photo)
            }
            println("response:" + photosList)
            val listView = findViewById<ListView>(R.id.main_list_view)
            listView.adapter = ListViewAdapter(this, photosList)
        }

    }

    private class ListViewAdapter(context: Context, plist: ArrayList<Photos>) : BaseAdapter() {
        private val mInflator: LayoutInflater = LayoutInflater.from(context)
        private val list: ArrayList<Photos> = plist

        override fun getCount(): Int {
            return list.size
        }

        override fun getItem(position: Int): Any {
            return list[position]
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {
            val view: View?
            val viewHolder: ListRowHolder
            if (convertView == null) {
                view = this.mInflator.inflate(R.layout.list_view, parent, false)
                viewHolder = ListRowHolder(view)
                view.tag = viewHolder
            } else {
                view = convertView
                viewHolder = view.tag as ListRowHolder
            }

            // set title
            viewHolder.label.text = list[position].title

            // set image
            val url = URL(list[position].thumbnailUrl)
            ImageLoadTask(url.toString(), viewHolder.img).execute()

            return view
        }
    }

    private class ListRowHolder(row: View?) {
        public val label: TextView
        public val img: ImageView

        init {
            this.label = row?.findViewById(R.id.label) as TextView
            this.img = row?.findViewById(R.id.imgAvatar) as ImageView
        }
    }

}
